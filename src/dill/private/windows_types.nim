type
    handle* = pointer
    dword* = culong
    ProcessSecurtyAccessRights = enum
        ProcessTerminate = 0x0001
        ProcessCreateThread = 0x0002
        ProcessVMOperation = 0x0008
        ProcessVMRead = 0x0010
        ProcessVMWrite = 0x0020
        ProcessDupHandle = 0x0040
        ProcessCreateProcess = 0x0080
        ProcessSetQuota = 0x0100
        ProcessSetInformation = 0x0200
        ProcessQueryInformation = 0x0400
        ProcessSuspendResume = 0x0800
        ProcessQueryLimitedInformation = 0x1000
        Delete = 0x00010000
        ReadControl = 0x00020000
        WriteDac = 0x00040000
        WriteOwner = 0x00080000
        StandardRightsRequired = 0x000F0000
        Syncronize = 0x00100000
        ProcessAllAccess = StandardRightsRequired.int or Syncronize.int or 0xFFFF #0xFFF for windows.vista <?

converter psar2dword*(psar:ProcessSecurtyAccessRights):dword =
    return psar.dword

proc `or`*(psar_1,psar_2:ProcessSecurtyAccessRights):dword =
    psar_1.dword or psar_2.dword