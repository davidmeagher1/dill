# This is just an example to get you started. A typical library package
# exports the main API in this file. Note that you cannot rename this file
# but you can remove it if you wish.
import importc

proc upperCaseFirstLetter(str:string):string = #this is an idea for a mangling procedure
    result = str
    result[0] = (result[0].int - 32).char

type
    SecurityAttributes = object
        nlength:culong
        lpSecurityDescriptor:pointer
        bInheritHandle:bool
    SecurityAttributesR = ref SecurityAttributes

importC "windows.h" as tt
tt.mangle(upperCaseFirstLetter)
tt.implement:
    proc getWindowThreadProcessId(h:pointer,lpdword:ref culong):culong 
    proc findWindowA(lpClassName,lpWindowName:cstring):pointer 
    proc messageBox(hWnd:pointer,lpText,lpCaption:cstring,uType:uint):int 
    proc virtualAllocEx(hProcess:pointer,lpAdress:pointer,dwSize:BiggestUint,flAllocationType:culong,flProtect:culong):pointer 
    proc writeProcessMemory(hProcess:pointer,lpAddress:pointer,lpBuffer:cstring,nsize:BiggestUint,lpNumberOfBytesWritten:ref BiggestUint):bool 
    proc createRemoteThread(hProcess:pointer,lpThreadAttributes:SecurityAttributesR,dwStackSize:BiggestUint,lpStartAddress:pointer,lpParameter:pointer,dwCreationFlags:culong,lpThreadId:ref culong):pointer 
    proc loadLibraryA(lpLibFileName:cstring):pointer 
    proc closeHandle(hObject:pointer):bool 
    proc virtualFreeEx(hProcess:pointer,lpAdress:pointer,dwSize:BiggestUint,dwFreeType:culong):bool
    proc openProcess(dwDesiredAccess:culong,bInheritHandle:bool,dwProcessId:culong):pointer

var ProcessAllAccess {.importc:"PROCESS_ALL_ACCESS",header:"windows.h".}:culong
const MemReserve:culong = 0x00002000
const MemCommit:culong = 0x00001000
const MemRelease:culong = 0x00008000
const MaxPath:culong = 260
const PageExecuteReadWrite = 0x40

proc post_error*(title,msg:cstring):void =
    discard messageBox(nil,msg,title,0)
    quit(-1)

proc getProcId(windowTitle:string,processId: ref culong):void =
    echo getWindowThreadProcessId(findWindowA(nil,windowTitle),processId)

when isMainModule:
    import os,strformat
    var proc_id:ref culong = new culong
    const dllName:string = "./test.dll"
    var dllPath:string = dllName.absolutePath
    const windowTitle:string = "Untitled - Paint"
    echo "here"
    if (not fileExists(dllpath)):
        post_error("fileExists",&"DLL: {dllPath} doesn't exist")
    else:
        echo dllPath
    getProcId(windowTitle,proc_id)
    if(proc_id == nil):
       post_error("getProcId",&"Failed to get Process ID from: {windowTitle}")
    else:
        echo proc_id.repr
    var h_process:pointer = openProcess(PROCESS_ALL_ACCESS,false,proc_id[])
    if(h_process == nil):
        post_error("openProcess","Failed to get handle to process")
    var allocated_memory:pointer = virtualAllocEx(h_process,nil,MaxPath,(MemReserve or MemCommit), PageExecuteReadWrite)

    if allocated_memory == nil:
        post_error("VirtualAllocEx","Failed to allocated memory")
    if (not writeProcessMemory(h_Process,allocated_memory,dll_Path,MaxPath,nil).bool):
        post_error("writeProcessMemory", "Failed to write process memory")
    
    var h_thread:pointer = createRemoteThread(h_process,nil,0,pointer(loadLibraryA),allocated_memory,0,nil)
    
    if h_thread == nil:
        post_error("createRemoteTHread","Failed")
    

    discard closeHandle(h_process)
    discard virtualFreeEx(h_process,allocated_memory,0,MEM_RELEASE)
    discard messageBox(nil,"SUCCESFULLY INJECTED","GOPT IT: " & $ProcessAllAccess,0)
