# Package

version       = "0.1.0"
author        = "David Meagher"
description   = "A library for dll injection in windows"
license       = "MIT"
srcDir        = "src"



# Dependencies

requires "nim >= 1.2.6"
